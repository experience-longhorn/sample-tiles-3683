using Microsoft.Windows.Client.Sidebar;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Presenters;

namespace SampleTiles
{
	public class BuildVersionTile : BaseTile
	{
		public BuildVersionTile() : base()
		{
		}

		public override void Initialize(IModuleViewHost mvh)
		{
			base.Initialize(mvh);
			mvh.SetHeaderInfo("Longhorn Version", null, null);
		}

		public override bool HasView(ModuleViewTypes view)
		{
			// This method allows the Sidebar to query what types of view
			// are supported by the Tile. Since this is a simple demo, we
			// are only going to support the "SideBar" view, which is just
			// the tile appearing in the sidebar
			return view == ModuleViewTypes.SideBar;
		}
	
		public override Element GetView(ModuleViewTypes view)
		{
			// For unsupported views, we just return null
			if(view != ModuleViewTypes.SideBar)
			{
				return null;
			}

			// Here we build up the UI. In later variants, this would be
			// done via XAML, and it's probably possible to do that here
			// as well with a bit more work, but the included tiles don't
			// use XAML, and neither will we for this demo.
			Element root = new DockPanel();
			
			Text versionLabel = new Text();
			versionLabel.SetValue(DockPresenter.DockProperty, Dock.Fill);
			versionLabel.SetValue(Presenter.MarginProperty, new BoxUnitThickness(10f));
			versionLabel.SetValue(TextPresenter.FontFamilyProperty, "Tahoma");
			versionLabel.SetValue(TextPresenter.FontSizeProperty, new BoxUnit(14f, UnitTypes.Point));
			versionLabel.SetValue(TextPresenter.TextAlignProperty, TextAlign.Center);
			versionLabel.SetValue(TextPresenter.ForegroundProperty, new SolidColorPaint(Colors.White));
			versionLabel.Nodes.Add(Environment.OSVersion.Version.ToString());
			root.Nodes.Add(versionLabel);

			return root;
		}
	}
}
