# Sample Sidebar Tiles for Longhorn
## Build 3683.Lab06_n

This project has been tested and developed on Build 3683.Lab06_n. It has not yet been tested on other builds, but I would imagine it'd work OK with at least most M3 builds.

### Building
This project file is targetted for Visual Studio 2003 / .NET Framework 1.1. You should be able to simply open the project within VS on a 3683 install and building the project. There is a post-build event that copies the DLL to the root of the `%USERPROFILE%` folder and reloads explorer.exe. If the DLL already exists, it will rename it first so as to prevent file-in-use errors if you have `explorer.exe` open and using the tile.

### Loading
You will also need to add these registry entries.

```
Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\StartBar\Modules\VersionTile]
"Assembly"="SampleTiles"
"Module"="SampleTiles.BuildVersionTile"
```


### Notes
The loading process in 3683 is pretty simple, it will simply look for .NET assemblies at the following location, based on the `Assembly` registry value:
* `%CD%\<Assembly>.dll`
* `%WINDIR%\<Assembly>.dll`
* `%WINDIR%\<Assembly>.exe`
* `%WINDIR%\<Assembly>\<Assembly>.dll`
* `%WINDIR%\<Assembly>\<Assembly>.exe`

The current directory for a typical `explorer.exe` process is `%USERPROFILE%`, i.e. `C:\Documents and Settings\User`.